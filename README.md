# README #

Plymouth Manager written in PyQt5 for Arch Linux and its derivates. 
Including VGA GRUB Hook modules, and GRUB Resolution settings.

### Application Overview ###

* install/remove plymouth
![plymouthize.png](https://bitbucket.org/repo/x8R8M4r/images/3500227357-plymouthize.png)

* select/install/remove/apply plymouth themes
![plymouthize2.png](https://bitbucket.org/repo/x8R8M4r/images/2509524232-plymouthize2.png)

![plymouthize3.png](https://bitbucket.org/repo/x8R8M4r/images/3219620169-plymouthize3.png)

### How to install: ###

clone repo and run (without sudo)

makepkg -sci


### Bad check sums? ###

updpkgsums
