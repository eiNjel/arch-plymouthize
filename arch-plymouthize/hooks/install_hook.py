# -*- coding: utf-8 -*-
#!/usr/bin/env python2
########################################################################################
#   Project: Arch Plymouthize
#   Manage Plymouth installation/themes with ease
#   Author: eiNjel <alekw3@gmail.com>
#   Copyright: 2015 eiNjel
#   License: GPL-3+
#
#  This program is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation; either version 3 of the License, or (at your option)
#  any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with this program.
#  If not, see <http://www.gnu.org/licenses/>.
#
#  GNU General Public License for Arch Plymouthize can be found
#  in the file /usr/share/licenses/arch-plymouthize.
#######################################################################################
import os
import shutil
import glob
import re
import subprocess


def mkinit_command():
    check = subprocess.Popen('uname -r', shell=True, stdin=subprocess.PIPE,
                             stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    if 'gnu' in check.stdout.read().split('\n')[0]:
        return 'mkinitcpio -p linux-libre'
    else:
        return 'mkinitcpio -p linux'


def get_dm_service():
    dm = glob.glob('/sys/fs/cgroup/systemd/system.slice/*dm*.service')
    dm1 = str(dm).strip('[]')
    if str(dm1) == "":
        dm = os.path.isfile('/sys/fs/cgroup/systemd/system.slice/slim.service')
        if dm is True:
            service = "slim.service"
            return service
    else:
        dm = re.sub("'", '', dm1)
        service1 = dm.split("/")[-1]
        service = service1.split("-")[0]
        return service


def set_new_dm_service(old_dm):
    old_dm_temp = str(old_dm.split('.')[0])
    sufix = "-plymouth"
    new_dm = old_dm_temp+sufix
    return new_dm


dm_service = str(get_dm_service())
new_service = str(set_new_dm_service(dm_service))
shutil.copyfile("/etc/default/grub", "/tmp/grub.backup")
#### adding plymouth to hooks and grub, executing cmds
os.system("sed -i 's/quiet/quiet splash/g' /tmp/grub.backup")
### copying files back to where they should be
os.system("cp /tmp/grub.backup /etc/default/grub")
os.system("grub-mkconfig -o /boot/grub/grub.cfg")
#### removing installation files
os.system("rm -rf /tmp/plymouth")
os.system("rm -f /tmp/plymouth.tar")
os.system("rm -f /tmp/grub.backup")

if dm_service != 'sddm.service':
    os.system("systemctl disable " + dm_service + "")
    os.system("systemctl enable " + new_service + "")

#not needed cmds from older version of arch plymouthize

shutil.copyfile("/etc/mkinitcpio.conf", "/tmp/mkinitcpio.conf.backup")
#### this line is in case you tryed setting plymouth in HOOK before and failed xD
os.system("sed -i 's/plymouth //g' /tmp/mkinitcpio.conf.backup")
os.system("sed -i 's/keymap /keymap plymouth /g' /tmp/mkinitcpio.conf.backup")
#os.system("gksudo cp /tmp/mkinitcpio.conf.backup /etc/mkinitcpio.conf")
#os.system("gksudo '%s'" % mkinit_command())
os.system("cp /tmp/mkinitcpio.conf.backup /etc/mkinitcpio.conf")
os.system("%s" % mkinit_command())
os.system("rm -f /tmp/mkinitcpio.conf.backup")
