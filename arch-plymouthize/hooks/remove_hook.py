# -*- coding: utf-8 -*-
#!/usr/bin/env python2
########################################################################################
#   Project: Arch Plymouthize
#   Manage Plymouth installation/themes with ease
#   Author: eiNjel <alekw3@gmail.com>
#   Copyright: 2015 eiNjel
#   License: GPL-3+
#
#  This program is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation; either version 3 of the License, or (at your option)
#  any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with this program.
#  If not, see <http://www.gnu.org/licenses/>.
#
#  GNU General Public License for Arch Plymouthize can be found
#  in the file /usr/share/licenses/arch-plymouthize.
#######################################################################################
import os
import shutil
import glob
import re
import subprocess


def mkinit_command():
    check = subprocess.Popen('uname -r', shell=True, stdin=subprocess.PIPE,
                             stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    if 'gnu' in check.stdout.read().split('\n')[0]:
        return 'mkinitcpio -p linux-libre'
    else:
        return 'mkinitcpio -p linux'


def get_dm_service():
    dm = glob.glob('/sys/fs/cgroup/systemd/system.slice/*dm*.service')
    dm1 = str(dm).strip('[]')
    if str(dm1) == "":
        dm = os.path.isfile('/sys/fs/cgroup/systemd/system.slice/slim-plymouth.service')
        if dm is True:
            service = "slim-plymouth.service"
            return service
    else:
        dm = re.sub("'", '', dm1)
        service1 = dm.split("/")[-1]
        service = service1.split(".")[0]
        return service


def set_new_dm_service(old_dm):
    old_dm_temp = str(old_dm.split('-')[0])
    sufix = ".service"
    new_dm = old_dm_temp
    return new_dm

dm_service = str(get_dm_service())
new_service = str(set_new_dm_service(dm_service))
#### restoring configuration to files
shutil.copyfile("/etc/default/grub", "/tmp/grub.tmp")
os.system("sed -i 's/ splash//g' /tmp/grub.tmp")
os.system("cp /tmp/grub.tmp /etc/default/grub")
os.system("grub-mkconfig -o /boot/grub/grub.cfg")
#### deleting tmp files
os.system("rm -f /tmp/grub.tmp")

os.system("systemctl disable "+ dm_service +"")
os.system("systemctl enable "+ new_service +"")

## old sys config from arch plymouthize
shutil.copyfile("/etc/mkinitcpio.conf", "/tmp/mkinitcpio.backup")
os.system("sed -i 's/ plymouth//g' /tmp/mkinitcpio.backup")
#os.system("gksudo cp /tmp/mkinitcpio.backup /etc/mkinitcpio.conf")
#os.system("gksudo '%s'" % mkinit_command())
#os.system("gksudo 'sed -i 's/ShowDelay/#ShowDelay/g' /etc/plymouth/plymouthd.conf'")
os.system("cp /tmp/mkinitcpio.backup /etc/mkinitcpio.conf")
os.system("%s" % mkinit_command())
os.system("sed -i 's/ShowDelay/#ShowDelay/g' /etc/plymouth/plymouthd.conf")
os.system("rm -f /tmp/mkinitcpio.backup")
