# -*- coding: utf-8 -*-
#!/usr/bin/env python2
########################################################################################
#   Project: Arch Plymouthize
#   Manage Plymouth installation/themes with ease
#   Author: eiNjel <alekw3@gmail.com>
#   Copyright: 2015 eiNjel
#   License: GPL-3+
#
#  This program is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation; either version 3 of the License, or (at your option)
#  any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with this program.
#  If not, see <http://www.gnu.org/licenses/>.
#
#  GNU General Public License for Arch Plymouthize can be found
#  in the file /usr/share/licenses/arch-plymouthize.
#######################################################################################
import os
import subprocess


def mkinit_command():
    check = subprocess.Popen('uname -r', shell=True, stdin=subprocess.PIPE,
                             stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    if 'gnu' in check.stdout.read().split('\n')[0]:
        return 'mkinitcpio -p linux-libre'
    else:
        return 'mkinitcpio -p linux'


os.system("sed -i 's/MODULES=/#MODULES=/g' /etc/mkinitcpio.conf")
os.system("echo '' >> /etc/mkinitcpio.conf")
os.system("cat /tmp/current_vga.conf >> /etc/mkinitcpio.conf")
os.system("%s" % mkinit_command())
